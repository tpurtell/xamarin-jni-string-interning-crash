﻿using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Thread = Java.Lang.Thread;

namespace AndroidApplication4
{
    [Activity(Label = "StringInterningCorruption", MainLauncher = true, Icon = "@drawable/icon")]
    public class StringInterningCorruption : Activity
    {
        int count = 1;
        private Button button;
        private static readonly  string[] keys =
            new string[] { "1"};

        private int pending = 0;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            button = FindViewById<Button>(Resource.Id.MyButton);

            button.Click += delegate { button.Text = string.Format("{0} clicks!", count++);};

            new Thread(() => Really("a")).Start();
            new Thread(() => Really("b")).Start();

            new Thread(() =>
            {
                var r = new Random();
                for (; ; )
                {
                    GC.Collect();
                    Thread.Sleep(r.Next(5));
                }
            }).Start();
        }

        private void Really(string v)
        {
            var jd = new JavaDictionary<string, string>();
            foreach (var s in keys)
                jd.Add(s, v);
            for (; ; )
            {
                foreach (var s in jd.Keys) //crash in here (iterator java.lang.string => string most likely)
                {
                    if (s != "1")
                        throw new InvalidProgramException(); //never thrown
                }
                if (count++%100 < 2)
                {
                    Interlocked.Increment(ref pending);
                    Application.SynchronizationContext.Post(o =>
                    {
                        Interlocked.Decrement(ref pending);
                        button.Text = string.Format("count {0} - pending {1}", count, pending);
                    }, null);
                }
            }
        }
    }
}

